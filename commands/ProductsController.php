<?php

namespace app\commands;

use app\services\ProductsService;
use yii\console\Controller;
use yii\console\ExitCode;

class ProductsController extends Controller
{
    /**
     * php -f yii products/generate-thumbnails 100 1 1
     */
    public function actionGenerateThumbnails(string $sizes, bool $watermarked = false, bool $catalogOnly = true): int
    {
        $sizes = explode( ',', $sizes);
        $sizesFormatted = [];
        foreach ($sizes as $size) {
            if (!preg_match('/^(([1-9]\d{0,3})|([1-9]\d{0,3})x([1-9]\d{0,3}))$/', $size, $matches)) {
                echo "Wrong input data\n";

                return ExitCode::DATAERR;
            }

            if (is_numeric($size)) {
                $sizesFormatted[] = [
                    'width' => $size,
                    'height' => $size,
                ];
            } else {
                [$width, $height] = explode( 'x', $size);
                $sizesFormatted[] = [
                    'width' => $width,
                    'height' => $height,
                ];
            }
        }

        $result = ProductsService::generateThumbnails($sizesFormatted, $watermarked, $catalogOnly);

        echo("Generated: {$result['generatedImages']}\n");
        echo("Not Generated: {$result['notGeneratedImages']}\n");

        return ExitCode::OK;
    }
}

