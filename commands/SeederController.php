<?php

namespace app\commands;

use tebazil\yii2seeder\Seeder;
use yii\console\Controller;

class SeederController extends Controller
{

    /** php -f yii seeder/generate */
    public function actionGenerate()
    {
        $seeder    = new Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker     = $generator->getFakerConfigurator();

        $seeder->table('product')->columns([
            'id',
            'is_deleted' => $faker->boolean()
        ])->rowQuantity(10);

        $seeder->table('store_product')->columns([
            'id',
            'product_id' => $generator->relation('product', 'id'),
        ])->rowQuantity(3);

        $seeder->refill();

        \Yii::$app->db->createCommand("UPDATE {{%product}} SET image = CONCAT(id, '.jpg');")->execute();
    }
}
