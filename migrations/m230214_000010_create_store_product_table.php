<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%store_product}}`.
 */
class m230214_000010_create_store_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%store_product}}', [
            'id'            => $this->primaryKey(),
            'product_id'    => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx_product_id', 'store_product', 'product_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%store_product}}');
    }
}

