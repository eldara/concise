<?php

return [
    'watermark' => [
        'name'      => 'watermark.png',
        'position'  => 'top-left',
        'positionX' => 0,
        'positionY' => 0,
    ],
];
