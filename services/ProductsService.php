<?php

namespace app\services;

use app\models\Product;
use yii\base\Exception;

class ProductsService
{
    public static function generateThumbnails(array $sizesFormatted, bool $watermarked, bool $catalogOnly): array
    {
        $query = Product::find()->andWhere(['is_deleted' => 0]);
        if ($catalogOnly) {
            $query = $query->innerJoinWith('storeProduct');
        }
        $products = $query->all();

        $generatedImages    = 0;
        $notGeneratedImages = 0;

        foreach ($products as $product) {
            foreach ($sizesFormatted as $sizes) {
                try {
                    if ($watermarked) {
                        ImagesService::generateWatermarkedMiniature($product->getImagePath(), $sizes);
                    } else {
                        ImagesService::generateMiniature($product->getImagePath(), $sizes);
                    }
                    $generatedImages++;
                } catch (Exception $exception) {
                    $notGeneratedImages++;
                }
            }
        }

        return [
            'generatedImages'    => $generatedImages,
            'notGeneratedImages' => $notGeneratedImages,
        ];
    }
}
