<?php

namespace app\services;

use Yii;
use Intervention\Image\ImageManagerStatic;
use yii\base\Exception;

class ImagesService
{
    public static function getThumbImagePath(string $imagePath, array $sizes, $watermarked = false): string
    {
        $pathinfo = pathinfo($imagePath);
        $postfix = '_' . $sizes['width'] . 'x' . $sizes['height'] . ($watermarked ? '_watermarked' : '');
        return $pathinfo['dirname'] . '/' .  $pathinfo['filename'] . $postfix . '.' . $pathinfo['extension'];
    }

    /** @throws Exception */
    public static function generateMiniature(string $imagePath, array $sizes): string
    {
        if (!file_exists($imagePath)) {
            throw new Exception('Error while generating watermark');
        }

        $thumbImagePath = self::getThumbImagePath($imagePath, $sizes);

        ImageManagerStatic::make($imagePath)->fit($sizes['width'], $sizes['height'])->save($thumbImagePath);

        if (!file_exists($thumbImagePath)) {
            throw new Exception('Error while generating watermark');
        }

        return $thumbImagePath;
    }

    /** @throws Exception */
    public static function generateWatermarkedMiniature(string $imagePath, array $sizes): string
    {
        $thumbImagePath = self::getThumbImagePath($imagePath, $sizes, true);

        if (!file_exists($imagePath)) {
            throw new Exception('Error while generating watermark');
        }

        if (empty(Yii::$app->params['watermark'])) {
            throw new Exception('Error while generating watermark');
        }

        $image = ImageManagerStatic::make($imagePath)->fit($sizes['width'], $sizes['height']);

        $watermarkPath = Yii::getAlias('@app') . '/web/images/' . Yii::$app->params['watermark']['name'];

        $image->insert(
            $watermarkPath,
            Yii::$app->params['watermark']['position'],
            Yii::$app->params['watermark']['positionX'],
            Yii::$app->params['watermark']['positionY']
        );

        $image->save($thumbImagePath);

        if (!file_exists($thumbImagePath)) {
            throw new Exception('Error while generating watermark');
        }

        return $thumbImagePath;
    }
}
