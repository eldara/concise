## Запуск проекта

1. Clone repo

   ```git clone git@gitlab.com:eldara/concise.git```

2. Run composer  

   ```sudo composer install```

3. Creade database

   ```CREATE DATABASE concise CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;```

4. Run migrations

   ```php yii migrate```

5. Add test data to db

    ```php -f yii seeder/generate```

6. Generate thumbnails

   ```php -f yii products/generate-thumbnails 100,200x200 1 0```
